import Category from "./Category.js"

class Command {
    getName() {
        return ""
    }

    getAliases() {
        return []
    }

    getDescription() {
        return ""
    }

    getCategory() {
        return Category.None
    }

    run(client, message, args, send, db, prefix) {

    }
}

export default Command