export default Object.freeze({
    None: 0,
    General: 1,
    TextResponse: 2,
    Fun: 3
})