import Command from "../structs/Command.js"
import Category from "../structs/Category.js"
import ytSearch from "youtube-search"
import attachmentUtils from "../utils/attachmentUtils.js"
import { ulid } from "ulid"
import Axios from "axios"

class YouTubeSearchCommand extends Command {
    getName() {
        return "ytsearch"
    }

    getAliases() {
        return ["yt", "yts"]
    }

    getDescription() {
        return "Displays the first result from a YouTube search, straight from the official YouTube API"
    }

    getCategory() {
        return Category.TextResponse
    }

    async run(client, message, args, send, db, prefix) {
        if (!process.env.YOUTUBE_API_KEY) send("No YouTube API key configured in `.env`.\n" +
            "Please contact the bot administrator to either add one or disable the command.")

        try {
            const search = await ytSearch(args.join(" "), {
                key: process.env.YOUTUBE_API_KEY,
                maxResults: 1,
                type: "video"
            })

            const video = search.results[0]

            /**
             * Very naive and poor way of escaping a title.
             * @todo "Very naive and poor way" — replace with a better way
             * @param {String} title The title
             * @returns The escaped title
             */
            const escapeTitle = (title) => {
                return title.replaceAll("*", "[*]").replaceAll("\\", "[\\]").replaceAll("@", "[@]")
            }

            const tnRes = await Axios.get(
                // Pick best resolution
                (video.thumbnails.maxres?.url ?? video.thumbnails.high?.url ?? video.thumbnails.default?.url),
                { responseType: "arraybuffer" })
            const tnBuffer = Buffer.from(tnRes.data)

            const attachment = await attachmentUtils.bytesToAttachment(tnBuffer, ".jpg")

            client.channels.sendMessage(message.channel, {
                content: `The first result was:\n\n${video.link}\n> **${escapeTitle(video.title)}**\n*${video.channelTitle}*`,
                nonce: ulid(),
                attachment
            })
        } catch (e) {
            console.error(e)
            send("An error occurred. " + e)
        }

    }
}

export default YouTubeSearchCommand