import Command from "../structs/Command.js"
import Category from "../structs/Category.js"
import { unslugify } from "unslugify"
import markdownUtils from "../utils/markdownUtils.js"

class InfoCommand extends Command {
    getName() {
        return "info"
    }

    getAliases() {
        return []
    }

    getDescription() {
        return "Displays general bot information"
    }

    getCategory() {
        return Category.General
    }

    run(client, message, args, send, db, prefix) {
        const table = markdownUtils.mkObjectTable({
            "version": "v0.0.0",
            "repository": "Soon™",
            "based": "$\\color{lime}\\textsf{true}$"
        }, unslugify)

        send("# Alpine\n\n" + table)
    }
}

export default InfoCommand