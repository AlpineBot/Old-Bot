import Command from "../structs/Command.js"
import Category from "../structs/Category.js"
import app from "../app.js"
import { markdownTable } from "markdown-table"

class HelpCommand extends Command {
    getName() {
        return "help"
    }

    getAliases() {
        return []
    }

    getDescription() {
        return "Displays a list of categories, and a list of commands for each category"
    }

    getCategory() {
        return Category.General
    }
    // ChannelGroupJoin

    run(client, message, args, send, db, prefix) {
        const commandObject = Object.fromEntries(app.commands)
        const commandNames = Object.keys(commandObject)
        const commandClasses = Object.values(commandObject)
        const commandLength = commandNames.length

        const commandInfo = [["Command", "Aliases", "Category", "Description"]]

        for (let i = 0; i < commandLength; i++) {
            const cmd = commandClasses[i]
            commandInfo.push([
                cmd.getName(), // Command
                (cmd.getAliases().length ? cmd.getAliases().join(", ") : "$\\color{gray}\\textsf{none}$"), // Aliases
                Object.keys(Category)[cmd.getCategory()], // Category
                cmd.getDescription() // Description
            ])
        }

        const table = markdownTable(commandInfo, {
            align: ["l", "l", "l", "l"] // left align all
        })

        send(`## Help\n${table}`)
    }
}

export default HelpCommand