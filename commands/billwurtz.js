import Command from "../structs/Command.js"
import Category from "../structs/Category.js"
import Axios from "axios"

class BillWurtzCommand extends Command {
    getName() {
        return "billwurtz"
    }

    getAliases() {
        return ["bw", "bill", "hotewig"]
    }

    getDescription() {
        return "Displays a random «history of the entire world, i guess» quote"
    }

    getCategory() {
        return Category.TextResponse
    }

    run(client, message, args, send, db, prefix) {
        Axios.get("https://history.geist.ga/api/one").then(response => {
            if (response.data?.result) {
                send(`$$
\\textsf{Bill Wurtz says:}\\\\
\\color{#FD6671} \\Huge{\\textsf{${response.data.result}}}
$$`)
            } else send("No result from API")
        }).catch(e => { send(`[API] ${e}`) })
    }
}

export default BillWurtzCommand