import Command from "../structs/Command.js"
import Category from "../structs/Category.js"

const rickRolls = [
    "https://www.youtube.com/watch?v=soq9cLVa5Xo",
    "https://www.youtube.com/watch?v=HV8IxKtAlrA",
    "https://www.youtube.com/watch?v=eBsIpefmXvE",
    "https://www.youtube.com/watch?v=dQw4w9WgXcQ"
]

class RickRollCommand extends Command {
    getName() {
        return "rickroll"
    }

    getAliases() {
        return ["rr", "rick"]
    }

    getDescription() {
        return "It's a rickroll!"
    }

    getCategory() {
        return Category.TextResponse
    }

    run(client, message, args, send, db, prefix) {
        const item = Math.floor(Math.random() * rickRolls.length)
        send("check this out:\n" + rickRolls[item] + "\n\n$\\tiny\\text{this command was suggested I have no idea why it's useful}$")
    }
}

export default RickRollCommand