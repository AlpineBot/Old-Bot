/**
 * Formats a packet.
 * @param {import("revolt.js/dist/websocket/notifications").ClientboundNotification} packetObject Packet
 */
const formatPacket = (packetObject) => {
    // TODO process certain packet types and include additional info for them

    let out = `\nPacket (${new Date().toLocaleString()}) [${packetObject.type}]\n`

    out += "[Raw]\n" + JSON.stringify(packetObject, null, 2)

    out += "\n"
    return out
}

export default {
    formatPacket
}