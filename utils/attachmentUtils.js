import { instanceInfo } from "../app.js"
import FormData from "form-data"
import { ulid } from "ulid"
import { createReadStream, promises as fsp } from "fs"
import Axios from "axios"

/**
 * Uploads a file to autumn.
 * @param {Buffer} bytes Buffer of data.
 * @param {String} ext File extension, **including the leading dot** (if needed).
 * @returns Autumn file URL
 */
const bytesToAttachment = async (bytes, ext) => {
    if (!instanceInfo?.features?.autumn?.enabled || !instanceInfo?.features?.autumn?.url)
        throw new Error("Autumn is not enabled or misconfigured")

    const filename = "__temporary-" + ulid() + ext

    const file = await fsp.writeFile(filename, bytes)

    const fileStream = createReadStream(filename)

    const formData = new FormData()

    formData.append("file", fileStream)

    const response = await Axios.post(instanceInfo.features.autumn.url, formData, {
        headers: formData.getHeaders()
    })

    const json = response.data

    fileStream.close()
    fsp.rm(filename)

    return json.id
}

export default {
    bytesToAttachment
}