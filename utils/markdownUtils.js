const mkObjectTable = (object, unslugifier = (x) => x) => {
    let head = "|"
    let separator = "|"
    let body = "|"

    const keys = Object.keys(object)
    const values = Object.values(object)

    for (let i = 0; i < keys.length; i++) {
        let unslugifiedKey = unslugifier(keys[i])
        let space = Math.max(unslugifiedKey.length, values[i].length) + 2
        head += ` ${unslugifiedKey}`.padEnd(space, " ") + "|"
        separator += "-".repeat(space) + "|"
        body += ` ${values[i]}`.padEnd(space, " ") + "|"
    }

    return `${head}\n${separator}\n${body}`
}

export default {
    mkObjectTable
}