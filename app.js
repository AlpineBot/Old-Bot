import Revolt from "revolt.js"
import { ulid } from "ulid"
import fs from "fs"
import path from "path"
import dirname from "es-dirname"
import ioRedis from "ioredis"
import dotenv from "dotenv"
import packets from "./utils/packetUtils.js"
import Axios from "axios"

dotenv.config()

export const instanceInfo = (await Axios.get(process.env.REVOLT_API)).data

const db = new ioRedis(process.env.REDIS_PORT, process.env.REDIS_HOST, {})

db.on("error", (e) => {
    console.error(`Database ${e}`)
})

const client = new Revolt.Client()

const mkNonce = () => ulid()

const commands = new Map()
const aliases = new Map()

client.on("dropped", () => {
    console.error("Dropped")
})

let commandCount = 0

client.on("message", async message => {
    if (message.channel !== process.env.TESTING_CHANNEL) return

    const prefix = (await db.get(message.channel + ":prefix")) ?? "!"
    if (!message.content.startsWith(prefix)) return
    if (prefix !== "!!" && message.content.startsWith("!!")) return

    const parts = message.content.split(" ")
    const cmd = parts[0].substr(prefix.length)
    const args = parts.slice(1)

    const sendMsg = (content) => {
        client.channels.sendMessage(message.channel, {
            content: content,
            nonce: mkNonce()
        })
    }

    if (commands.has(cmd)) {
        commands.get(cmd).run(client, message, args, sendMsg, db, prefix)
    } else if (aliases.has(cmd)) {
        commands.get(aliases.get(cmd)).run(client, message, args, sendMsg, db, prefix)
    } else return sendMsg("## Error\n\nCommand not found")
})

client.on("packet", (packet) => {
    if (process.env.LOG_ALL_PACKETS)
        fs.appendFileSync("packetlog.txt", packets.formatPacket(packet))
})

client.on("ready", async () => {
    const channelCount = client.channels.toArray().length - 1 // minus saved messages

    console.log(`Ready!
    - as @${client.user.username}
    - in ${channelCount} channels`)

    console.log(`Updating database counts for channels (${channelCount}) and commands (${commandCount})`)
    await db.set("count:channels", channelCount)
    await db.set("count:commands", commandCount)
})

const commandDir = fs.readdirSync(path.join(dirname(), "commands"))
commandDir.forEach(async cmdFile => {
    const cmd = (await import(`./commands/${cmdFile}`)).default

    console.log("Loading", cmdFile)

    const cmdInstance = new cmd()

    if (cmdInstance.getAliases().length) {
        cmdInstance.getAliases().forEach(alias => {
            aliases.set(alias, cmdInstance.getName())
        })
    }

    commandCount++

    commands.set(cmdInstance.getName(), cmdInstance)
})

if (fs.existsSync("packetlog.txt")) {
    console.log("Deleting old packet log")
    fs.rmSync("packetlog.txt")
}

client.useExistingSession({
    session_token: process.env.SESSION_TOKEN,
    id: process.env.USER_ID,
    user_id: process.env.USER_ID
})

export default {
    commands,
    instanceInfo
}